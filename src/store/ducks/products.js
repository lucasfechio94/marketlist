const Types = {
    ADD_PRODUCT: 'products/ADD_PRODUCT',
    DELETE_PRODUCT: 'products/DELETE_PRODUCT',
    DELETE_ALL_PRODUCTS: 'products/DELETE_ALL_PRODUCTS',
    CHANGE_PRODUCT_AMOUNT: 'products/CHANGE_PRODUCT_AMOUNT',
    CHANGE_PRODUCT_STATUS: 'products/CHANGE_PRODUCT_STATUS',
    SET_REGISTERED_PRODUCTS: 'products/SET_REGISTERED_PRODUCTS',
}

const initialState = {
    products: [
        { name: 'Arroz', amount: 5, wasAdd: false },
        { name: 'Feijão', amount: 2, wasAdd: false },
        { name: 'Açúcar', amount: 1, wasAdd: false },
        { name: 'Café', amount: 1, wasAdd: false },
        { name: 'Macarrão', amount: 3, wasAdd: false },
    ],
    registeredProducts: [],
}

export function addProduct(prod) {
    return {
        type: Types.ADD_PRODUCT,
        payload: prod,
    }
}

export function deleteProduct(idx) {
    return {
        type: Types.DELETE_PRODUCT,
        payload: idx,
    }
}

export function deleteAllProducts() {
    return {
        type: Types.DELETE_ALL_PRODUCTS,
    }
}

export function changeAmount(idx, amount) {
    return {
        type: Types.CHANGE_PRODUCT_AMOUNT,
        payload: { idx, amount },
    }
}

export function changeStatus(idx, status) {
    return {
        type: Types.CHANGE_PRODUCT_STATUS,
        payload: { idx, status },
    }
}

export function setRegisteredProducts(data) {
    return {
        type: Types.SET_REGISTERED_PRODUCTS,
        payload: data,
    }
}

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case Types.ADD_PRODUCT: {
            return { ...state, products: [...state.products, action.payload] }
        }
        case Types.DELETE_PRODUCT: {
            const newProducts = [
                ...state.products.slice(0, action.payload), ...state.products.slice(action.payload + 1),
            ]
            return { ...state, products: newProducts }
        }
        case Types.DELETE_ALL_PRODUCTS: {
            return { ...state, products: [] }
        }
        case Types.CHANGE_PRODUCT_AMOUNT: {
            const newProducts = [...state.products]
            newProducts[action.payload.idx].amount = action.payload.amount
            return { ...state, products: newProducts }
        }
        case Types.CHANGE_PRODUCT_STATUS:{
            const newProducts = [...state.products]
            newProducts[action.payload.idx].wasAdd = action.payload.status
            return { ...state, products: newProducts }
        }
        case Types.SET_REGISTERED_PRODUCTS: {
            return { ...state, registeredProducts: action.payload }
        }
        default:
            return state
    }
}