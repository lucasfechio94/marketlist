import React from 'react'
import PropTypes from 'prop-types'
import { useDispatch } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import { Check, Delete } from '@material-ui/icons'
import { deleteProduct, changeStatus, changeAmount } from '../store/ducks/products'
import {
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
    TextField,
} from '@material-ui/core'

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
    deleteIcon: {
        color: 'red',
        cursor: 'pointer',
    },
    checkIcon: {
        color: 'green',
        cursor: 'pointer',
        marginRight: '10px'
    },
    amountField: {
        width: '100px',
    },
});

const TableList = React.memo(({ columns, rows }) => {
    const classes = useStyles()
    const dispatch = useDispatch()
    
    return (
        <TableContainer component={Paper}>
            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        {
                            columns.map(col => (
                                <TableCell key={col.id}>{col.value}</TableCell>
                            ))
                        }
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row, idx) => (
                        <TableRow key={row.name}>
                            <TableCell
                                align='left'
                                style={{ textDecoration: row.wasAdd && 'line-through' }}
                            >
                                {row.name}
                            </TableCell>
                            <TableCell align='left'>
                                <TextField
                                    type='number'
                                    variant='outlined'
                                    className={classes.amountField}
                                    value={row.amount}
                                    onChange={e => dispatch(changeAmount(idx, parseInt(e.target.value)))}
                                />
                            </TableCell>
                            <TableCell align='left'>
                                <Check
                                    className={classes.checkIcon} 
                                    onClick={() => dispatch(changeStatus(idx, !row.wasAdd))}
                                />
                                <Delete
                                    className={classes.deleteIcon}
                                    onClick={() => dispatch(deleteProduct(idx))}
                                />
                            </TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
})

TableList.propTypes = {
    columns: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.string,
            value: PropTypes.string,
        })
    ).isRequired,
    rows: PropTypes.arrayOf(
        PropTypes.shape({
            name: PropTypes.string,
            amount: PropTypes.number,
            wasAdd: PropTypes.bool,
        })
    ).isRequired,
}

export default TableList