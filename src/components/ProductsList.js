import React, { useState, useEffect, useCallback } from 'react'
import PropTypes from 'prop-types'
import { useSelector, useDispatch } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import getProducts from '../gateways/products'
import { setRegisteredProducts } from '../store/ducks/products'
import { AddCircleOutline, Search } from '@material-ui/icons'
import Modal from './Modal'
import {
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    TextField,
    Button,
} from '@material-ui/core'


const useStyles = makeStyles({
    form: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
    },
    addIcon: {
        color: 'green',
        cursor: 'pointer',
    },
    searchButton: {
        color: 'blue',
    },
    clearButton: {
        color: 'red',
    },
});


const ProductsList = React.memo(({ handleSelect, handleClose }) => {
    const classes = useStyles()
    const dispatch = useDispatch()
    const [open, setOpen] = useState(false)
    const [filter, setFilter] = useState('')
    const [loading, setLoading] = useState(false)
    const [filteredRows, setFilteredRows] = useState([])
    const { registeredProducts } = useSelector(state => {
        return state.products
    })

    const fetchData = useCallback(async () => {
        setLoading(true)
        try {
            const { data } = await getProducts()
            dispatch(setRegisteredProducts(data))
            setFilteredRows(data)
            setLoading(false)
        } catch {
            setOpen(true)
        }
    }, [dispatch])

    useEffect(() => {
        // caso já tenha dado o fetch, não será feito novamente
        registeredProducts.length === 0
            ? fetchData()
            : setFilteredRows(registeredProducts)
    }, [fetchData, registeredProducts])

    const handleSubmit = useCallback((event) => {
        event.preventDefault()
        // o filtro será feito ao dar submit pois dessa forma fica
        // mais rápido do que seria em tempo real
        filter
            ? setFilteredRows(registeredProducts.filter(row => row.description.toLowerCase().includes(filter.toLowerCase())))
            : setFilteredRows(registeredProducts)
    }, [filter, registeredProducts])

    const handleClear = useCallback(() => {
        setFilteredRows(registeredProducts)
        setFilter('')
    }, [registeredProducts])

    const handleAdd = useCallback((value) => {
        handleSelect(value)
        handleClose()
    }, [handleSelect, handleClose])

    return (
        <>
            <form className={classes.form} onSubmit={handleSubmit}>
                <TextField
                    label='Pesquisar'
                    variant='outlined'
                    value={filter}
                    onChange={e => setFilter(e.target.value)}
                />
                <Button
                    type='submit'
                    className={classes.searchButton}
                >
                    <Search />
                </Button>
                <Button
                    className={classes.clearButton}
                    onClick={handleClear}
                >
                    Limpar Busca
                </Button>
            </form>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell >Produto</TableCell>
                        <TableCell />
                    </TableRow>
                </TableHead>
                <TableBody>
                    {
                        loading
                            ? (
                                <TableRow>
                                    <TableCell>Carregando produtos...</TableCell>
                                </TableRow>
                            )
                            : (filteredRows.map(row =>
                                <TableRow key={row.id}>
                                    <TableCell
                                        align='left'
                                        style={{ textDecoration: row.wasAdd && 'line-through' }}
                                    >
                                        {row.description}
                                    </TableCell>
                                    <TableCell>
                                        <AddCircleOutline
                                            className={classes.addIcon}
                                            onClick={() => handleAdd(row.description)}
                                        />
                                    </TableCell>
                                </TableRow>))
                    }
                </TableBody>
            </Table>
            <Modal
                open={open}
                handleClose={() => setOpen(false)}
            >
                Erro ao carregar os produtos
            </Modal>
        </>
    )
})

ProductsList.propTypes = {
    handleSelect: PropTypes.func.isRequired,
    handleClose: PropTypes.func.isRequired,
}

export default ProductsList