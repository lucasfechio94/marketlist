import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { Link } from 'react-router-dom'
import {
    AppBar,
    Toolbar,
    Typography
} from '@material-ui/core'

const useStyles = makeStyles(({
    root: {
        flexGrow: 1,
    },
    title: {
        cursor: 'pointer'
    },
    link: {
        color: '#fff',
        padding: '20px',
        textDecoration: 'none'
    },
}))

const Menu = () => {
    const classes = useStyles()

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant='h6' className={classes.title}>
                        <Link className={classes.link} to='/'> Home </Link>
                        <Link className={classes.link} to='/list'> Lista </Link>
                    </Typography>
                </Toolbar>
            </AppBar>
        </div>
    );
}

export default Menu