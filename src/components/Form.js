import React, { useState, useCallback } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { TextField, Button } from '@material-ui/core'
import { AddCircleOutline, Search, ClearAll } from '@material-ui/icons'
import { makeStyles } from '@material-ui/core/styles'
import { addProduct, deleteAllProducts } from '../store/ducks/products'
import Modal from './Modal'
import ProductsList from './ProductsList'

const useStyles = makeStyles({
    form: {
        width: '100%',
        marginBottom: '20px',
    },
    productField: {
        width: '100%',
    },
    amountField: {
        width: '100%',
    },
    button: {
        height: '100%',
        backgroundColor: 'transparent',
        marginLeft: '10px',
    },
    addButton: {
        color: 'green',
    },
    searchButton: {
        color: 'blue',
    },
    clearButton: {
        color: 'red',
    },
    main: {
        display: 'flex',
        flexDirection: 'row',

        '@media (max-width: 980px)': {
            flexDirection: 'column',
        },
    },
    textField: {
        width: '50%',
        marginRight: '10px',

        '@media (max-width: 980px)': {
            width: '100%',
            marginBottom: '20px',
        },
    },
    actions: {
        display: 'flex',
        width: '50%',

        '@media (max-width: 980px)': {
            width: '100%',
            alignItems: 'center',
        },

        '@media (max-width: 380px)': {
            flexDirection: 'column',
        },
    },
    actionsButtons: {
        display: 'flex',
        flexDirection: 'row',

        '@media (max-width: 380px)': {
            marginTop: '20px',
        },
    }
});

const Form = () => {
    const classes = useStyles()
    const dispatch = useDispatch()
    const [name, setName] = useState('')
    const [amount, setAmount] = useState(0)
    const [open, setOpen] = useState(false)
    const [openErrorModal, setOpenErrorModal] = useState(false)
    const { products } = useSelector(state => {
        return state.products
    })

    const handleSubmit = useCallback((event) => {
        event.preventDefault()

        // verifica as seguintes condições para salvar o item:
        // 1 - já existe um produto com o mesmo nome na lista;
        // 2 - o nome não é vazio;
        // 3 - a quantidade é maior que 0.
        if (products.every(prod => prod.name !== name) && name && (amount > 0)) {
            dispatch(addProduct(
                {
                    name,
                    amount: parseInt(amount),
                    wasAdd: false
                }
            ))
            setName('')
            setAmount(0)
        } else {
            setOpenErrorModal(true)
        }
    }, [amount, dispatch, name, products])

    return (
        <>
            <form className={classes.form} onSubmit={handleSubmit}>
                <div className={classes.main}>
                    <div className={classes.textField}>
                        <TextField
                            className={classes.productField}
                            label='Produto'
                            variant="outlined"
                            value={name}
                            onChange={e => setName(e.target.value)}
                        />
                    </div>
                    <div className={classes.actions}>
                        <TextField
                            className={classes.productField}
                            label='Quantidade'
                            type='number'
                            variant='outlined'
                            value={amount}
                            onChange={e => setAmount(e.target.value)}
                        />
                        <div className={classes.actionsButtons}>
                            <Button
                                type='submit'
                                className={`${classes.button} ${classes.addButton}`}
                            >
                                <AddCircleOutline />
                            </Button>
                            <Button
                                className={`${classes.button} ${classes.searchButton}`}
                                onClick={() => setOpen(true)}
                            >
                                <Search />
                            </Button>
                            <Button
                                className={`${classes.button} ${classes.clearButton}`}
                                onClick={() => dispatch(deleteAllProducts())}
                            >
                                <ClearAll />
                            </Button>
                        </div>
                    </div>
                </div>
            </form>
            <Modal
                open={open}
                handleClose={() => setOpen(false)}
            >
                <ProductsList
                    handleSelect={setName}
                    handleClose={() => setOpen(false)}
                />
            </Modal>
            <Modal
                open={openErrorModal}
                handleClose={() => setOpenErrorModal(false)}
            >
                Verifique se o produto já está na lista e  se
                todos os campos do formulário foram preenchidos
            </Modal>
        </>
    )
}

export default Form