import React from 'react'
import PropTypes from 'prop-types'
import { 
    Button,
    Dialog,
    DialogActions,
    DialogContent
} from '@material-ui/core'

const Modal = React.memo(({ open, handleClose, children }) => {
    return (
        <>
            <Dialog
                open={open}
                onClose={handleClose}
            >
                <DialogContent>
                    {children}
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color='primary'>
                        Fechar
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    );
})

Modal.propTypes = {
    open: PropTypes.bool.isRequired,
    handleClose: PropTypes.func.isRequired,
    children: PropTypes.node.isRequired,
}

export default Modal