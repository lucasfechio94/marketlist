import React from 'react'
import Home from '../views/Home'
import List from '../views/List'
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from 'react-router-dom'

export default () => (
    <Router>
        <Switch>
            <Route path='/' component={Home} />
            <Route path='/list' component={List} />
        </Switch>
    </Router>
)