import React from 'react'
import { Provider } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import Container from '@material-ui/core/Container'
import List from './views/List'
import Home from './views/Home'
import Menu from './components/Menu'
import store from './store'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

const useStyles = makeStyles({
  container: {
    height: '100vh',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '50px',
  },
});

const App = () => {
  const classes = useStyles()

  return (
    <Provider store={store}>
      <Router>
        <Menu />
        <Container className={classes.container} fixed>
          <Switch>
            <Route path='/' exact component={Home} />
            <Route path='/list' component={List} />
          </Switch>
        </Container>
      </Router>
    </Provider>
  );
}

export default App;
