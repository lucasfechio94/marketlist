import axios from 'axios'

const proxyurl = 'https://cors-anywhere.herokuapp.com/'
const url = 'https://taco-food-api.herokuapp.com/api/v1/food'

export default function () {
    return axios.get(proxyurl + url)
}