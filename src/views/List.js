import React from 'react'
import { useSelector } from 'react-redux'
import Form from '../components/Form'
import TableList from '../components/TableList'

const columns = [
    { id: 'name', value: 'Nome' },
    { id: 'amount', value: 'Quantidade' },
    { id: 'action', value: 'Ação' },
]

const List = () => {
    const { products } = useSelector(state => {
        return state.products
    })
    return (
        <>
            <Form />
            <TableList
                rows={products}
                columns={columns}
            />
        </>
    )
}

export default List