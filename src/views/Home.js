import React from 'react'
import { Link } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import { Button } from '@material-ui/core'
import shoppingCart from '../assets/carrinho.png'

const useStyles = makeStyles({
    home: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    img: {
        width: '100%',
    },
    button : {
        marginTop: '50px',
        width: '200px',
    },
    link: {
        textDecoration: 'none',
        color: '#fff',
    },
});

const Home = () => {
    const classes = useStyles()

    return (
        <div className={classes.home}>
            <img className={classes.img} alt='' src={shoppingCart} />
            <Button
                className={classes.button}
                variant='contained'
                color='primary'
            >
                <Link to='/list' className={classes.link}>Fazer Lista</Link>
            </Button>
        </div>
    )
}

export default Home